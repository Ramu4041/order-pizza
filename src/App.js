import React, { Component } from 'react';
import './App.css'

const initialMinus = 'https://images.vexels.com/media/users/3/131484/isolated/preview/a432fa4062ed3d68771db7c1d65ee885-minus-inside-circle-icon-by-vexels.png';
const plusIcon = 'https://cdn0.iconfinder.com/data/icons/round-ui-icons/512/add_red.png';
const finalMinus = 'https://www.pinclipart.com/picdir/middle/13-137245_button-plus-and-minus-signs-plus-minus-sign.png';


class App extends Component {
    state = {
        sizeList: [
            {
                size: 'small',
                count: 0,
                prize: 150
            },
            {
                size: 'medium',
                count: 1,
                prize: 200
            },
            {
                size: 'large',
                count: 0,
                prize: 300
            }
        ],
        adults: 1,
        children: 0,

    }

    getTotal = () => {
        return this.state.sizeList.reduce((aggr, item) => {
            if (item.size === 'small') {
                aggr += item.count * 150
            } else if (item.size === 'medium') {
                aggr += item.count * 200
            } else {
                aggr += item.count * 300
            }
            return aggr;
        }, 0)
    }

    onAddPizza = (index) => {
        let sizeList = this.state.sizeList;
        let element = sizeList[index];
        let total = this.getTotal();
        let {
            children, adults
        } = this.state;
        if (element.size === 'small' && total < 851) {
            this.setState({ children: children += 1 })
            sizeList[index].count = sizeList[index].count += 1;
            this.setState({
                sizeList
            })
        } else if (element.size === 'medium' && total < 801 )  {
            this.setState({ adults: adults += 1 })
            sizeList[index].count = sizeList[index].count += 1;
            this.setState({
                sizeList
            })
        } else if (element.size === 'large' && total < 701) {
            this.setState({ adults: adults += 2 })
            sizeList[index].count = sizeList[index].count += 1;
            this.setState({
                sizeList
            })
        } 
    }

    onRemovePizza = (index) => {
        let sizeList = this.state.sizeList;
        let element = sizeList[index];
        let total = this.getTotal();
        let {
            children, adults
        } = this.state;
        if (element.size === 'small' && total > 349) {
            this.setState({ children: children -= 1 })
            sizeList[index].count = sizeList[index].count -= 1;
            this.setState({
                sizeList
            })
        } else if (element.size === 'medium' && total > 399) {
            this.setState({ adults: adults -= 1 })
            sizeList[index].count = sizeList[index].count -= 1;
            this.setState({
                sizeList
            })
        } else if (element.size === 'large' && total > 499) {
            this.setState({ adults: adults -= 2 })
            sizeList[index].count = sizeList[index].count -= 1;
            this.setState({
                sizeList
            })
        }
    }

    render() {
        return (
            <div className="dashboard-container" >
                <div className="page-content">
                    <h4 className="zero-margin blue-text">Order <span className="bold-text">Pizza</span></h4>
                    <div className="context-box">
                        <div className="context-box-content">
                            {
                                this.state.sizeList.map((item, index) => {
                                    return <div className='pizza-count'>
                                        <div className="pizza-box-content">
                                            <img alt='' className={`${item.size}-img`} src='https://cdn4.iconfinder.com/data/icons/fast-food-menu-blue-line/64/147_fast-food-menu-pizza-slice-512.png' />
                                            <p className='size-text blue-text'>{item.size}</p>
                                            <div className='pizza-count-content'>
                                                <img onClick={() => { if (item.count !== 0) this.onRemovePizza(index) }} className='minus-img' alt='' src={item.count===0 ? initialMinus : finalMinus } />
                                                <p className='added-count'>{item.count}</p>
                                                <img className='plus-img' onClick={() => this.onAddPizza(index)} alt='' src={plusIcon} />
                                            </div>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                        <div className="category-row">
                            <p>ADULTS</p>
                            <div className='pizza-count-content'>
                                <img onClick={() => { if (this.state.adults !== 0) this.onRemovePizza(1) }} className='minus-img' alt='' src={this.state.adults === 0 ? initialMinus : finalMinus} />
                                <p className='added-count'>{this.state.adults}</p>
                                <img className='plus-img' onClick={() => this.onAddPizza(1)} alt='' src={plusIcon} />
                            </div>
                        </div>
                        <div className="category-row">
                            <p>CHILDREN</p>
                            <div className='pizza-count-content'>
                                <img onClick={() => { if (this.state.children !== 0) this.onRemovePizza(0) }} className='minus-img' alt='' src={this.state.children === 0 ? initialMinus : finalMinus} />
                                <p className='added-count'>{this.state.children}</p>
                                <img className='plus-img' onClick={() => this.onAddPizza(0)} alt='' src={plusIcon} />
                            </div>
                        </div>
                    </div>
                    <div className='total-container'>
                        <h4 className="zero-margin blue-text">Order <span className="bold-text">Total</span></h4>
                        <h4 className='zero-margin'>Rs {this.getTotal()}</h4>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;